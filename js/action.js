'use strict';

const today = document.querySelector('.today');
const region = document.querySelector('.today__region');
const time = document.querySelector('.today__time');
const todayImg = document.querySelector('.today__img');
const todayTemp = document.querySelector('.today__temp');
const daysList = document.querySelector('.days-list');
let locat;
let current;
let days = new Array();
const xhr = new XMLHttpRequest();
const url = 'http://api.weatherapi.com/v1/forecast.json?key=ec239642bd61486db2193003200109&q=Gomel&days=10';

today.addEventListener('click', drawToday);

xhr.open('GET', url);

xhr.onreadystatechange = function() {
    if (xhr.readyState === xhr.DONE) {
        if (xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            console.log(data)
            locat = data.location;
            current = data.current;

            days = converDayData(data.forecast.forecastday);
            drawToday();
            drawDays();

            console.log(days)
        }
    } 
}

xhr.send();

function drawToday() {
    region.innerHTML = locat.name;
    time.innerHTML = locat.localtime;
    todayImg.src = current.condition.icon;
    todayTemp.innerHTML = `${current.temp_c} <span>&#176;</span>`;
};

function drawDays() {
    daysList.innerHTML = '';
    days.forEach(item => {
        const dayTag = creatDayCard(item);
        daysList.appendChild(dayTag);
    });
}

function creatDayCard(days) {
    const day = document.createElement('div');
    day.classList.add('card__day');
    day.innerText = getDay(days.date);

    const image = document.createElement('img');
    image.classList.add('card__img');
    image.src = days.image;

    const temp = document.createElement('div');
    temp.classList.add('card__temp');
    const maxTemp = document.createElement('span');
    maxTemp.innerHTML = `${days.maxTemp} <span>&ordm;</span> `;
    const minTemp = document.createElement('span');
    minTemp.innerHTML = ` ${days.minTemp} <span>&ordm;</span>`;
    minTemp.style.color = 'grey';
    temp.appendChild(maxTemp);
    temp.appendChild(minTemp);

    const card = document.createElement('div');
    card.classList.add('card');
    const openHandler = () => openDay(days);
    card.addEventListener('click', openHandler);
    card.appendChild(day);
    card.appendChild(image);
    card.appendChild(temp);

    return card;
}

function openDay(days) {
    time.innerHTML = days.date;
    todayImg.src = days.image;
    todayTemp.innerHTML = `${days.maxTemp} <span>&#176;</span>`;
}

function converDayData(days) {
    return days.map(item => {
        return {
            date: item.date,
            image: item.day.condition.icon,
            maxTemp: item.day.maxtemp_c,
            minTemp: item.day.mintemp_c,
        }
    });
}

function getDay(unix) {
    let result;
    const date = new Date(unix);
    const weekDay = date.getDay();

    switch(weekDay) {
        case 1:
            result = 'ПН';
        break;
        case 2:
            result = 'ВТ';
        break;
        case 3:
            result = 'СР';
        break;
        case 4:
            result = 'ЧТ';
        break;
        case 5:
            result = 'ПТ';
        break;
        case 6:
            result = 'СБ';
        break;
        case 7:
            result = 'ВС';
        break;
    }

    return result;
}